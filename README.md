## Von-Neumann vs Hardvard

```plantuml
@startmindmap
* VS
** Hardvard

*** Que es? \n \n Este modelo, que utilizan los micro controladores PIC, tiene la unidad central de proceso \n conectada a dos memorias. \n Una de las memorias contiene solamente las instrucciones del programa \n (Memoria de Programa), \n y la otra sólo almacena datos.
*** Ventajas. \n \n 1.El tamaño de las instrucciones no está relacionado con el de los datos. \n 2. El tiempo de acceso a las instrucciones puede superponerse con el de los datos. \n 3. Las instrucciones y los datos se almacenan en caches separadas para mejorar el rendimiento. \n 4. Esta arquitectura suele utilizarse en DSPs, o procesador de señal digital, \n usados habitualmente en productos para procesamiento de audio y video.


left side

** Von-Neumann
*** Quien es? \n \n Nacido el 28 de diciembre de 1903 en Budapest,Hungría; \n fallecido el 8 de febrero de 1957 en Washington, DC. \n \n Grandes Contribuciones \n \n  1.30 julio desarrollo un programa que consistía en el almacenamiento de datos. \n 2.Creo la arquitectura de los computadores actuales. \n 3.Propuso el bit como elemento fundamental. \n 4.1943 von Neumann comenzó a trabajar en el Proyecto Manhattan.
*** Limitaciones \n \n 1.La limitación de la longitud de las instrucciones por el bus de datos. \n 2.La limitación de la velocidad de operación a causa del bus único para \n datos e instrucciones que no deja acceder simultáneamente a unos y otras.
*** Ventajas. \n \n 1.Los datos y las instrucciones, se almacenan en una misma memoria de lectura/escritura. \n 2.No se pueden diferenciar entre datos e instrucciones al examinar una posición de memoria. \n 3.Los contenidos de la memoria son direccionados por su ubicación, sin importar el tipo de datos contenido. \n 4.La ejecución ocurre en modo secuencial mediante la lectura de instrucciones consecutivas desde la memoria.

@endmindmap
```


## Supercomputadoras

```plantuml
@startmindmap
* Supercomputadoras
** Que son las supercomputadoras? 
*** El término se aplica comúnmente a los sistemas de alto rendimiento más rápidos disponibles en un momento dado, \n también una supercomputadora es una serie de computadoras que actúan como una máquina colectiva capaz de \n procesar enormes cantidades de datos.

** ¿Qué tareas realizan las supercomputadoras?
*** Las supercomputadoras están diseñadas para tareas computacionalmente intensivas que requieren una gran potencia de \n procesamiento y, las tareas complejas que a una computadora normal le tomaría años completar se pueden  realizar en \n una fracción del tiempo.
**** Estas pueden ser utilizadas en. 
***** 1.Investigación aerodinámica \n 2.Investigación climática \n 3.Criptoanálisis (descifrado de códigos) \n 4.Exploración de gas y petróleo \n 5.Modelado molecular.
** ¿Qué sistema operativo usan las supercomputadoras?
*** Todas las supercomputadoras más importantes de la actualidad utilizan un sistema operativo basado en Linux.
** Características de la supercomputadora.
*** 1.Pueden admitir más de cien usuarios a la vez. \n 2.Estas máquinas son capaces de manejar la enorme cantidad de cálculos \n que están más allá de las capacidades humanas, es decir, el humano es incapaz \n de resolver cálculos tan extensos. \n 3.Muchas personas pueden acceder a supercomputadoras al mismo tiempo. \n 4.Estas son las computadoras más caras que jamás se hayan fabricado.



left side

** Supercomputadoras ubicadas en Mexico
*** 1958 se tubo a primera computadora en Latinoamérica en la universidad de la UNAM, y \n era una IBM 650, que tenía una memoria de 2 cast, y esta funcionaba con bulbos
*** La computadora Kan Balam. \n \n Esta supercomputadora, estuvo en el edificio de la dirección general de computo y de la \n tecnología de información y comunicación en la UNAM, su uso estudios más solicitados era el \n estudio científicos y simbólicos. \n Fabricante: HP \n Capacidades: 3.016 GB de memoria RAM y 160 TB de almacenamiento, además de tener 7 teraflops \n y 1,368  procesadores AMD Opteron a 2.6 GHz.

*** La computadora Aitzaloa.\n Esta computadora se encuetra unicada en UAM Iztapalapa del año 2008 \n Capacidades: 3.016 GB de memoria RAM, 100 TB de almacenamiento, procesamiento de 18.4 teraflops, \n procesadores Intel Xeon E5272 QuadCore con 2,160 núcleos en conjunto. \n Usos: Proyectos en química cuántica ligados al diseño de nuevos materiales, simulación de fluidos a nivel molecular \n, simulación de tráfico en sistemas de telefonía celular, proyectos de astronomía sobre el movimiento de las galaxias, investigaciones, \n simulación para la búsqueda de yacimientos petroleros, entre otros.


*** Xiuhcoatl. \n La supercomputadora Xiuhcoatl, esta ubicada en CINVESTAV del año 2012 \n Fabricante: HP \n Capacidades: 11 TB de memoria RAM, 60 TB de almacenamiento, procesamiento de 50 teraflops, \n procesadores con 4,724 núcleos en conjunto y tarjetas gráficas que suman más de 374 mil núcleos. \n Usos: Estudios avanzados de física cuántica, astronomía, sismología, investigación y predicción del clima.


*** Abacus. \n La computadora Abacus está ubicada en CINVESTAV en el año 2014 \n Capacidades: 40 TB de memoria RAM, 1.2 PB de almacenamiento, procesamiento de 400 teraflops, procesadores Intel Xeon E5 con \n 8,904 núcleos en conjunto y 100 tarjetas gráficas K40 fabricadas por Nvidia. \n El uso más relevante de las supercomputadoras, se utilizan en la Investigación y modelación de problemas complejos.



@endmindmap
*** 
```
